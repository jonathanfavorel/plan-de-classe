﻿namespace plan_de_classr
{
    partial class ajoutclasse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ajoutclasse));
            this.label1 = new System.Windows.Forms.Label();
            this.nomclasse = new System.Windows.Forms.TextBox();
            this.addClasse = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "nom de la classe:";
            // 
            // nomclasse
            // 
            this.nomclasse.Location = new System.Drawing.Point(12, 55);
            this.nomclasse.Name = "nomclasse";
            this.nomclasse.Size = new System.Drawing.Size(129, 20);
            this.nomclasse.TabIndex = 1;
            this.nomclasse.TextChanged += new System.EventHandler(this.nomclasse_TextChanged);
            // 
            // addClasse
            // 
            this.addClasse.Location = new System.Drawing.Point(12, 98);
            this.addClasse.Name = "addClasse";
            this.addClasse.Size = new System.Drawing.Size(129, 23);
            this.addClasse.TabIndex = 2;
            this.addClasse.Text = "Ajouter";
            this.addClasse.UseVisualStyleBackColor = true;
            this.addClasse.Click += new System.EventHandler(this.addClasse_Click);
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(13, 127);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(129, 23);
            this.cancel.TabIndex = 3;
            this.cancel.Text = "Annuler";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // ajoutclasse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(154, 172);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.addClasse);
            this.Controls.Add(this.nomclasse);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ajoutclasse";
            this.Text = "ajoutclasse";
            this.Load += new System.EventHandler(this.ajoutclasse_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button addClasse;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.TextBox nomclasse;
    }
}