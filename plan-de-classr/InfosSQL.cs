﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;

namespace plan_de_classr
{
    class InfosSQL
    {
        public static MySqlConnection GetDBConnection()
        {
            string host = "localhost";
            int port = 3306;
            string database = "plan_de_classe";
            string username = "root";
            string password = "";

            return ConnexionMySql.GetDBConnection(host, port, database, username, password);
        }
    }
}