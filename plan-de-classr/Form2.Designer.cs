﻿namespace plan_de_classr
{
    partial class Form2
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_modify = new System.Windows.Forms.Button();
            this.listClasse = new System.Windows.Forms.ListBox();
            this.ChoixClasse = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AjoutCLasse = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(12, 331);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(87, 34);
            this.btn_add.TabIndex = 4;
            this.btn_add.Text = "Ajouter";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(260, 331);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(87, 34);
            this.btn_delete.TabIndex = 5;
            this.btn_delete.Text = "supprimer";
            this.btn_delete.UseVisualStyleBackColor = true;
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_modify
            // 
            this.btn_modify.Location = new System.Drawing.Point(140, 331);
            this.btn_modify.Name = "btn_modify";
            this.btn_modify.Size = new System.Drawing.Size(87, 34);
            this.btn_modify.TabIndex = 6;
            this.btn_modify.Text = "modifier";
            this.btn_modify.UseVisualStyleBackColor = true;
            this.btn_modify.Click += new System.EventHandler(this.btn_modify_Click);
            // 
            // listClasse
            // 
            this.listClasse.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listClasse.FormattingEnabled = true;
            this.listClasse.ItemHeight = 16;
            this.listClasse.Location = new System.Drawing.Point(12, 12);
            this.listClasse.Name = "listClasse";
            this.listClasse.Size = new System.Drawing.Size(344, 292);
            this.listClasse.TabIndex = 8;
            this.listClasse.SelectedIndexChanged += new System.EventHandler(this.listClasse_SelectedIndexChanged);
            // 
            // ChoixClasse
            // 
            this.ChoixClasse.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChoixClasse.FormattingEnabled = true;
            this.ChoixClasse.ItemHeight = 16;
            this.ChoixClasse.Location = new System.Drawing.Point(370, 83);
            this.ChoixClasse.Name = "ChoixClasse";
            this.ChoixClasse.Size = new System.Drawing.Size(224, 212);
            this.ChoixClasse.TabIndex = 9;
            this.ChoixClasse.SelectedIndexChanged += new System.EventHandler(this.ChoixClasse_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(366, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(233, 20);
            this.label1.TabIndex = 10;
            this.label1.Text = "Sélectionner la classe a afficher";
            // 
            // AjoutCLasse
            // 
            this.AjoutCLasse.Location = new System.Drawing.Point(371, 331);
            this.AjoutCLasse.Name = "AjoutCLasse";
            this.AjoutCLasse.Size = new System.Drawing.Size(223, 34);
            this.AjoutCLasse.TabIndex = 11;
            this.AjoutCLasse.Text = "Ajouter une classe";
            this.AjoutCLasse.UseVisualStyleBackColor = true;
            this.AjoutCLasse.Click += new System.EventHandler(this.AjoutCLasse_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 382);
            this.Controls.Add(this.AjoutCLasse);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ChoixClasse);
            this.Controls.Add(this.listClasse);
            this.Controls.Add(this.btn_modify);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_add);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form2";
            this.Text = "plan de classe";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_modify;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AjoutCLasse;
        public System.Windows.Forms.ListBox ChoixClasse;
        public System.Windows.Forms.ListBox listClasse;
    }
}

