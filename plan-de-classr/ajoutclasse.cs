﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace plan_de_classr
{
    public partial class ajoutclasse : Form
    {
        

        public ajoutclasse()
        {
            InitializeComponent();
        }

        private void addClasse_Click(object sender, EventArgs e)
        {
            MySqlConnection connectionSQL = InfosSQL.GetDBConnection();
            connectionSQL.Open();

            if (nomclasse.Text != "")
            {
                MySqlCommand reset = new MySqlCommand("ALTER TABLE classe AUTO_INCREMENT = 0", connectionSQL);
                MySqlCommand ajout = new MySqlCommand("INSERT INTO `classe` (`id`, `nom`) VALUES (NULL, '" + nomclasse.Text + "')", connectionSQL);
                reset.ExecuteNonQuery();
                ajout.ExecuteNonQuery();
                string classe10 = nomclasse.Text;
                MessageBox.Show("Classe ajouté avec succès");
                Form2 add6 = new Form2();

                Form.ActiveForm.Close();
                connectionSQL.Close();
            }
            if (nomclasse.Text == "")
            {
                MessageBox.Show("veuillez entrer un nom de classe");
            }
        }
        private void cancel_Click(object sender, EventArgs e)
        {
            Form.ActiveForm.Close();
        }

        public void ajoutclasse_Load(object sender, EventArgs e)
        {

        }

        public void nomclasse_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
