﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace plan_de_classr
{
    public partial class Form1 : Form
    {


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MySqlConnection conn = InfosSQL.GetDBConnection();

            string Query = "select nom from salle";

            MySqlCommand cmdDatabase = new MySqlCommand(Query, conn);

            MySqlDataReader myreader;
            try
            {

                conn.Open();

                myreader = cmdDatabase.ExecuteReader();

                while (myreader.Read())
                {
                    Button Btn = new Button();

                    this.Controls.Add(Btn);
                    string Nom = myreader.GetString("nom");
                    Btn.Text = Nom;
                    tableLayoutPanel1.Controls.Add(Btn);
                    Btn.Click += Btn_click;


                }
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur;" + err);
            }

        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (textBox4.Text != "")
            {
                MySqlConnection conn = InfosSQL.GetDBConnection();

                //string Query = "UPDATE salle SET nom = 'nouvelle valeur ' WHERE condition;";
                string Query = "INSERT INTO salle (nom)" +
                    "VALUES (" + textBox4.Text + ");";

                MySqlCommand cmdDatabase = new MySqlCommand(Query, conn);

                MySqlDataReader myreader;
                try
                {

                    conn.Open();

                    myreader = cmdDatabase.ExecuteReader();
                    MessageBox.Show("La salle " + textBox4.Text + " à été ajouter");
                    Button Btn = new Button();
                    Btn.Text = textBox4.Text;
                    tableLayoutPanel1.Controls.Add(Btn);
                    conn.Close();

                    
    
                }
                catch (Exception err)
                {
                    MessageBox.Show("Erreur;" + err);
                }
            }
            else
            {
                MessageBox.Show("Veuillez rentrez un nom de salle");

            }

        }


        private void Label1_Click(object sender, EventArgs e)
        {
        }

        
        private void Supprimer_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                MySqlConnection conn = InfosSQL.GetDBConnection();



               // var controls = tableLayoutPanel1.Controls.Find(textBox1.Text, true);

                //if (controls != null)
               // {
                    //string Query = "UPDATE salle SET nom = 'nouvelle valeur ' WHERE condition;";/
                    
                    string Query = "DELETE FROM salle WHERE nom ='" + textBox1.Text + "';";


                    MySqlCommand cmdDatabase = new MySqlCommand(Query, conn);

                    MySqlDataReader myreader;

                    try
                    {

                        conn.Open();

                        myreader = cmdDatabase.ExecuteReader();


                       
                        MessageBox.Show("La salle " + textBox1.Text + " à été supprimer");
                    this.Hide();
                    Form1 form1 = new Form1();
                    form1.Show();
                    

                    }
                    catch (Exception err)
                    {
                        MessageBox.Show("Erreur;" + err);
                    }
                  //  }
                
                }
            else
            {
                MessageBox.Show("Veuillez rentrez le nom de la salle a supprimer");

            }

}

        

        private void button1_Click(object sender, EventArgs e)
        {
            //Form3 form3 = new Form3();
            //form3.Show();
            //this.Hide();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

       

        private void Btn_click(object sender, EventArgs e)
        {
            
            Form2 form2 = new Form2();
            form2.Show();
            this.Hide();
        }
        private void Modifier_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != "" && textBox3.Text != "")
            {
                    MySqlConnection conn = InfosSQL.GetDBConnection();

                //string Query = "UPDATE salle SET nom = 'nouvelle valeur ' WHERE condition;";/
                string Query = "UPDATE salle SET nom = '" + textBox3.Text + " ' WHERE nom='"+ textBox2.Text + "'; ";

                MySqlCommand cmdDatabase = new MySqlCommand(Query, conn);

                MySqlDataReader myreader;
                try
                {

                    conn.Open();

                    myreader = cmdDatabase.ExecuteReader();
                    MessageBox.Show("La salle " + textBox1.Text + " à été modifier");
                    this.Hide();
                    Form1 form1 = new Form1();
                    form1.Show();

                }
                catch (Exception err)
                {
                    MessageBox.Show("Erreur;" + err);
                }
            }
                else
                {
                    MessageBox.Show("Veuillez rentrez l'ancien nom ou le nouveau nom de la salle a modifier");
                 
                }

        }
    }
}
    
