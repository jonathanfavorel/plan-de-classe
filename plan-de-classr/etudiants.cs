﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace plan_de_classr
{
    public class etudiants
    {
        public string prenom;
        public string nom;
        public int age;
        public int idClasse;

        public etudiants(string nom, string prenom, int age, int idClasse)
        {
            this.prenom = prenom;
            this.nom = nom;
            this.age = age;
            this.idClasse = idClasse;
        }

        public override string ToString()
        {
            return nom.ToString();
        }
    }
}
